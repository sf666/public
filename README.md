# B2

1. Код проекта расположен по адресу:
https://gitlab.com/sf666/b2

2. Для доступа к коду проекта необходимо:
    1. Зарегистрировться на gitlab.com
    2. Добавить свой ssh ключ в профиль https://gitlab.com/-/profile/keys
    3. Дать мне имя своей учетки в гитлабе

3. Настройка среды разработки:
    1. Ввести следующие команды, указав свои учетные данные:
        - git config --global user.name **USERT_NAME**
        - git config --global user.email **EMAIL**
        - git config --global alias.history "log --graph1"
    2. В итоге, в профиле, в файле .gitconfig должно получиться что-то похожее на:
        - https://gitlab.com/sf666/b2/-/blob/main/.gitconfig

4. Процесс внесения изменений в кодовую базу:
    1. Синхронизируемся с актуальной версией ветки master:
        - git checkout master
        - git pull --all
    2. Создаем новую feature-ветку от master:
        - git checkout master
        - git checkout -b feature/**FEATURE_NAME**
    3. Вносим изменения в код.
    4. Пушим изменения в gitlab:
        - git add **CHANGED_FILES**
        - git commit -m "**NEW_CODE_PURPOSE**"
        - git push -u origin feature/**FEATURE_NAME**
    5. В gitlab создаем новый Merge Request из своей ветки в master:
        - https://gitlab.com/sf666/b2/-/merge_requests
        - В поле "Reviewer" указываем коллегу/лида.
        